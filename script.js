
let userNum = +prompt("Введіть ціле число");

while (!(Number.isInteger(userNum))) {
   userNum = +prompt("Введіть ціле(!) число")
}


let numSwitch = true;
function noNum() {
   console.log("Sorry, no numbers");
};

for (let i = 1; i <= userNum; i++) {
   if (userNum === 0) {
      break
   }
   if (i % 5 === 0) {
      console.log(i);
      numSwitch = false;
   }
}

if (numSwitch) {
   noNum()
};

//---------------Додаткове завдання---------------//

let m = +prompt("Введіть менше число");
let n = +prompt("Введіть більше чилсо");
while (!(Number.isInteger(m)) || !(Number.isInteger(n)) || (n < m)) {
   m = +prompt("Введіть ціле(!) менше число")
   n = +prompt("Введіть ціле(!) більше число")
}

for (let i = m; i <= n; i++) {
   for (let j = 2; j <= i; j++) {
      if (i % j === 0 && j < i) {
         break;
      } else if (j === i) {
         console.log(i);
      }
   }
}
